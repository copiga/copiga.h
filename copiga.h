/* VERSION 2*/
/*made by copiga*/

#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <ctype.h>

#define pi 3.14159265358979323			//sometimes it is nice to have pi already defined

_Bool randseeded = false;				//required for randomnumgen() to prevent dupes

int waitsecs(int waitlen){				//wait for waitlen seconds *NEEDS WORK, USES time.h AND CAN BE OFF BY UP TO 0.9 seconds*
	int A=waitlen+time(NULL);
	while (A > time(NULL)){}
	return 0;
}

int waitmins(int waitlen){				//wait for waitlen minutes **REQUIRES** waitsecs as it just multiplies the minutes by sixty
	waitlen = waitlen * 60;
	waitsecs(waitlen);
}

int randgen(int seed){					//returns a pseudorandom number seeded with time, pi and user input
	if (randseeded == false){					//seeds random with the time plus the seed given by the user multiplied by pi. but only if randseeded is false
	srand(time(NULL)+seed*pi);
	randseeded = true;
	}
	return rand();

}

int unseedrandgen(){					//sets randseeded to false for additional randomness USE SPARINGLY
	randseeded = false;
	if (randseeded == false){					//for testing, if return is zero then it worked, if not it failed
		return 0;
	} else {
		return  1;
	}
}

int scrclear(void)
{
#ifdef __linux__
  system("clear");
#elif _WIN32
  system("cls");
#endif
}


int clearin(void)
{
  /*may not work, use with great caution!*/
  /*seems to work after getchar but not scanf, may be rewritten as getinput()*/
  int removed = 0;
  while ((getchar())!= '\n')
    {
      removed++;
    }
  return removed;
}
